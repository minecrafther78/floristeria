export const colors = {
  primary: '#CCDBDC',
  primary_2: '#E7EEEE',
  secondary: '#80CED7',
  secondary_2: '#63C7B2',
  secondary_3: '#8E6C88',
  secondary_4: '#263D42'
}
