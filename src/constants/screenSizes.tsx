export const screenSizes = {
  mobile: '425px',
  tablet: '768px',
  laptop: '1024px',
  desktop: '2560px'
}
