/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { useEffect, useContext, useState } from 'react'
import { useNavigate } from 'react-router-dom'

import { getFlowerList } from '../../effects/flowerController'
import { Flower, FlowerList } from '../../types/types'

import { SearchBox } from '../../components/SearchBox/SearchBox'
import { FlowerCard } from '../../components/FlowerCard/FlowerCard'

import { FlowerListContainer, BoxContainer, SearchContainer, Container } from './styled'
import { AppContext } from '../../context'

export const Home = () => {
  const navigate = useNavigate()
  const appState = useContext(AppContext)
  const [filterList, setFilterList] = useState<FlowerList>([])
  const { list, setList } = appState

  const navigateToDetails = (flowerId: string) => {
    navigate(`/${flowerId}`)
  }

  useEffect(() => {
    const takeData = async () => {
      const data = await getFlowerList()
      setList(data)
      setFilterList(data)
    }
    takeData()
  }, [])

  const onFilterChange = (name: string) => {
    const newList = list.filter((flower) =>
      flower.name.toLowerCase().includes(name.toLowerCase()) ||
      flower.binomialName.toLowerCase().includes(name.toLowerCase())
    )
    setFilterList(newList)
  }

  return (
    <Container>
      <BoxContainer>
        <SearchContainer>
          <SearchBox onChange={onFilterChange} />
        </SearchContainer>
        <FlowerListContainer>
          {
            filterList.map((flower: Flower) => (
              <FlowerCard
                key={`flower_${flower.id}`}
                flower={flower}
                onCardClick={navigateToDetails}
              />
            ))
          }
        </FlowerListContainer>
      </BoxContainer>
    </Container>
  )
}
