import styled from 'styled-components'
import { screenSizes } from '../../constants/screenSizes'
import { colors } from '../../constants/colors'

export const Container = styled.div`
  width: 100%;
  height:100%;
  display: flex;
  justify-content: center;
`

export const BoxContainer = styled.div`
  background-color: ${colors.primary};
  font-family: sans-serif;
  line-height: 1.8;
  width: 100%;
  height:100%;
  padding-bottom: 2rem;
  display: flex;
  flex-direction: column;
  max-width: 1400px;
`

export const SearchContainer = styled.div`
  margin: 1.2rem;
  width: 90%;
  display: flex;
  flex-direction: row-reverse;
  justify-content: unset ;

  @media (max-width: ${screenSizes.tablet}) {
    justify-content: center;
  }
`

export const FlowerListContainer = styled.div`
  margin: 1.2rem;
  justify-items: center;
  width: 90%;
  display: grid;
  gap: 1.4rem;
  grid-auto-flow: dense;
  margin: 0 auto;
  grid-auto-rows: 1fr;
  grid-template-columns: repeat(auto-fill, minmax(15rem, 1fr));

  @media (min-width: ${screenSizes.laptop}) {
    grid-template-columns: 1fr 1fr 1fr 1fr;
  }

  justify-items: center;
`
