import styled from 'styled-components'
import { colors } from '../../constants/colors'

export const Container = styled.div`
  margin: 0;
  background-color: ${colors.primary};
  font-family: sans-serif;
  line-height: 1.8;
  width: 100%;
  padding-top: 4rem;
  align-content: center;
`
export const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row-reverse;
  margin: 1rem;
`

export const BackButton = styled.button`
  background-color: ${colors.secondary};
  border-radius: 8px;
  border: 2px solid transparent;
  box-sizing: border-box;
  color: #FFFFFF;
  cursor: pointer;
  display: inline-block;
  font-family: "Haas Grot Text R Web", "Helvetica Neue", Helvetica, Arial, sans-serif;
  font-size: 14px;
  font-weight: 500;
  height: 50px;
  line-height: 25px;
  list-style: none;
  margin: 0;
  outline: none;
  padding: 10px 16px;
  position: relative;
  text-align: center;
  text-decoration: none;
  vertical-align: baseline;
  user-select: none;
  -webkit-user-select: none;
  touch-action: manipulation;

  :hover {
    border: 2px solid ${colors.primary_2};
  }
`
