/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { useEffect, useContext } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import { getFlowerDetails } from '../../effects/flowerController'
import { AppContext } from '../../context'

import { FlowerDescription } from '../../components/FlowerDescription/FlowerDescription'
import { Container, BackButton, ButtonContainer } from './styles'

export const Details = () => {
  const params = useParams()
  const navigate = useNavigate()
  const appState = useContext(AppContext)

  const { id: flowerId } = params
  const { currentFlower, setCurrentFLower } = appState

  useEffect(() => {
    const takeData = async () => {
      if (flowerId !== undefined) {
        const data = await getFlowerDetails(flowerId)
        setCurrentFLower(data)
      }
    }
    takeData()

    return () => setCurrentFLower(undefined)
  }, [])

  const handleBackClick = () => {
    navigate('/')
  }

  if (currentFlower == null) return (<div />)

  return (
    <Container>
      <ButtonContainer>
        <BackButton onClick={handleBackClick}>Volver al inicio</BackButton>
      </ButtonContainer>
      <FlowerDescription flower={currentFlower} />
    </Container>
  )
}
