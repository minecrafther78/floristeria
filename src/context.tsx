import { createContext } from 'react'
import { Flower, FlowerList } from './types/types'

interface ContextInterface {
  list: FlowerList
  setList: any
  currentFlower: Flower | undefined
  setCurrentFLower: any
}

const initialState: ContextInterface = {
  list: [],
  setList: () => {},
  currentFlower: undefined,
  setCurrentFLower: () => {}
}
export const AppContext = createContext(initialState)
