/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { AxiosResponse } from 'axios'
import * as flowerService from '../api/flower'

export const getFlowerList = async () => {
  const res: AxiosResponse = await flowerService.getFlowerList()
  if (res.status === 200) return res.data
  return []
}

export const getFlowerDetails = async (id: string) => {
  const res: AxiosResponse = await flowerService.getFlowerDetails(id)
  if (res.status === 200) return res.data
  return {}
}
