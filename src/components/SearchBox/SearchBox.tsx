/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Search, Container } from './styles'

interface SearchBoxProps {
  onChange: (name: string) => void
}

export const SearchBox = (props: SearchBoxProps) => {
  const { onChange } = props

  return (
    <div>
      <Container>
        <Search onChange={(el) => onChange(el.target.value)} />
      </Container>
    </div>
  )
}
