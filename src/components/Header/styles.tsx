import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { colors } from '../../constants/colors'

const HEADER_HEIGHT = '70px'

export const Container = styled.div`
  height: ${HEADER_HEIGHT};
  justify-items: center;
  display: flex;
  flex-direction: row;
  background-color: ${colors.secondary_2};
`

export const Logo = styled.img`
  padding-left: 1rem;
  margin: 5px;
  width: 60px;
  height: 60px;

  :hover {
    cursor: pointer;
  }
`
export const StyledLink = styled(Link)`
  font-size: 1.2rem;
  margin-right: 5px;
  line-height: ${HEADER_HEIGHT};
  text-decoration: none;
  color: ${colors.secondary_4};

  :hover {
    font-weight:bold;
  }
`
