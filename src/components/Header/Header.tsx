/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Container, Logo, StyledLink } from './styles'
import { useContext } from 'react'
import { AppContext } from '../../context'

interface HeaderProps {
  onLogoClick: () => void
}
export const Header = (props: HeaderProps) => {
  const { onLogoClick } = props
  const appState = useContext(AppContext)
  const { currentFlower } = appState

  return (
    <Container>
      <Logo src='../../assets/logo.png' onClick={onLogoClick} />
      <StyledLink to='/'>Inicio</StyledLink>

      {
        Boolean(currentFlower) && (
          <StyledLink to={`${currentFlower.id}`}>/ {currentFlower.name}</StyledLink>
        )
      }
    </Container>
  )
}
