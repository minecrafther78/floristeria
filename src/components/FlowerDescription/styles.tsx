import styled from 'styled-components'
import { screenSizes } from '../../constants/screenSizes'
import { colors } from '../../constants/colors'

export const Container = styled.div`
  display: grid;
  background: ${colors.primary};
  grid-template-columns: 1fr 1fr;
  align-items: center;
  justify-items: center;
  height: 100%;
  width: 100%;

  @media (max-width: ${screenSizes.laptop}) {
    grid-template-columns: 1fr 1fr;
  }
  
  @media (max-width: ${screenSizes.tablet}) {
    grid-template-columns: 1fr;
  }
`

export const Title = styled.h1`
  font-size: 1.5em;
  font-weight: bold;
`

export const InfoContainer = styled.div`
  text-align: left;

  > p {
    font-size: 1.5em;
  }

  @media (max-width: ${screenSizes.tablet}) {
    width: 70%;
  }
`

export const Image = styled.img`
  display: block;
  border-radius: 0.6em;
  margin: 0 auto;
  width: 70%;
  height: 70vh;
  object-fit: fill;
  max-width: 700px;

  @media (max-width: ${screenSizes.tablet}) {
    height: 30vh;
  }

`
