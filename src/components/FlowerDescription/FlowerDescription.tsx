/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Flower } from '../../types/types'
import { Image, Container, InfoContainer, Title } from './styles'

interface FlowerDescriptionProps {
  flower: Flower
}
export const FlowerDescription = (props: FlowerDescriptionProps) => {
  const { flower } = props

  const {
    name,
    binomialName,
    price,
    imgUrl,
    wateringsPerWeek,
    fertilizerType,
    heightInCm
  } = flower

  return (
    <Container>
      <Image src={imgUrl} />

      <InfoContainer>
        <Title>{name}</Title>
        <p>Nombre científico: {binomialName}</p>
        <p>Riegos por semana: {wateringsPerWeek}</p>
        <p>Tipo de fertilización: {fertilizerType === 'nitrogen' ? 'Nitrógeno' : 'Fósforo'}</p>
        <p>Altura: {heightInCm}cm</p>
        <p>Precio: {price}€</p>
      </InfoContainer>
    </Container>
  )
}
