/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { Flower } from '../../types/types'
import { Card, Image, Title, Text } from './styles'

interface FlowerCardProps {
  flower: Flower
  onCardClick: (flowerId: string) => void
}

export const FlowerCard = (props: FlowerCardProps) => {
  const { flower, onCardClick: handleCardClick } = props
  const { name, price, imgUrl, id, binomialName } = flower

  return (
    <Card onClick={() => handleCardClick(id)}>
      <Image src={imgUrl} />
      <Title>{name}</Title>
      <Text>{binomialName}</Text>
      <Text>{price}€</Text>
    </Card>
  )
}
