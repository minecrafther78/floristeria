import styled from 'styled-components'
import { colors } from '../../constants/colors'

export const Card = styled.div`
  overflow: hidden;
  border-radius: 0.3rem;
  box-shadow: 0 0.2rem 0.2rem 0 ${colors.secondary_4};
  border: 2px solid transparent;
  background-color: ${colors.primary_2};
  width: 100%;
  max-width: 260px;
  
  :hover {
    cursor: pointer;
    box-shadow: 0 0.2rem 0.2rem 0 ${colors.secondary_2};
    border: 2px solid ${colors.secondary_2};
  }

  > p {
    padding-left: 1rem;
  }
`

export const Image = styled.img`
  object-fit: cover;
  object-position: center center;
  width: 100%;
  height: 200px;
`
export const Title = styled.p`
  font-size: 1.5em;
  font-weight: bold;
`

export const Text = styled.p`
  font-size: 1.2em;
`
