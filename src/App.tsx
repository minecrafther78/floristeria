/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { useState } from 'react'
import {
  Routes,
  Route,
  useNavigate
} from 'react-router-dom'
import { AppContext } from './context'
import { Header } from './components/Header/Header'
import { Home } from './pages/Home/Home'
import { Details } from './pages/Details/Details'

function App () {
  const navigate = useNavigate()
  const [list, setList] = useState([])
  const [currentFlower, setCurrentFLower] = useState(undefined)

  const handleLogoCLick = () => {
    navigate('/')
  }

  return (
    <AppContext.Provider value={{ list, setList, currentFlower, setCurrentFLower }}>
      <Header onLogoClick={handleLogoCLick} />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/:id' element={<Details />} />
      </Routes>
    </AppContext.Provider>
  )
}

export default App
