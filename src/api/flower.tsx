/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { FlowerList, Flower } from '../types/types'
import { axiosInstance } from './config'

export const getFlowerList = async () => await axiosInstance.get<FlowerList>('api/product')

export const getFlowerDetails = async (id: string) => await axiosInstance.get<Flower>(`api/product/${id}`)
