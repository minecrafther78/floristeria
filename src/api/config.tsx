import axios from 'axios'

export const axiosInstance = axios.create({
  baseURL: 'https://dulces-petalos.herokuapp.com/'
})
