export interface Flower {
  id: string
  name: string
  binomialName: string
  price: number
  imgUrl: string
  wateringsPerWeek: number
  fertilizerType: fertilizer
  heightInCm: number
}

export type fertilizer = 'nitrogen' | 'phosphorus'

export type FlowerList = Flower[]
